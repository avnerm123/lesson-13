#include <WinSock2.h>
#include <Windows.h>
#include "Helper.h"
#include <queue>
#include <mutex>
#include <exception>
#include <iostream>
#include <string>
#include <thread>

#define LOGINMSG 200
#define ServerUpdateMsg 101
#define ClientUpdateMsg 204

using std::vector;
using std::string;
using std::queue;
using std::pair;
using std::ifstream;
using std::ofstream;
using std::mutex;
using std::lock_guard;
using std::condition_variable;


class Server
{
public:
	Server();
	~Server();
	void serve(int port);
	void accept();

	
	
private:
	vector<string> Users;
	queue<pair<string, string>> msgQueue;
	
	void clientHandler(SOCKET clientSocket);

	SOCKET _serverSocket;
};
