#pragma comment (lib, "ws2_32.lib")

#include "Server.h"
#include <iostream>
#include <exception>
#include "WSAInitializer.h"

int main()
{
	std::cout << "starting.." << std::endl;
	try
	{
		WSAInitializer wsaInit;
		Server myServer;

		myServer.serve(8826);
	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	system("PAUSE");
	return 0;
}