
#include "Server.h"

mutex mtx;
condition_variable condVar;

void erase(std::vector<string>& v, string str)
{
	std::vector<string>::iterator iter = v.begin();

	while (iter != v.end())
	{
		if (*iter == str)
			iter = v.erase(iter);
		else
			iter++;
	}

}
Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	this->_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{
	int i = 0;
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	std::cout << "binded" << std::endl;
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening..." << std::endl;
	vector<std::thread> thread_pull;
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "accepting..." << std::endl;
		thread_pull.push_back(std::thread(&Server::accept,this));
		
		thread_pull[i].detach();
		std::this_thread::sleep_for(std::chrono::seconds(1));
		i++;
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;
	//std::thread ClientThread(&Server::clientHandler ,this , client_socket);
	// the function that handle the conversation with the client
	//ClientThread.detach();
	//std::this_thread::sleep_for(std::chrono::seconds(60));
	clientHandler(client_socket);
}


void Server::clientHandler(SOCKET clientSocket)
{
	std::string client;		
	
	char m[1024] = { 0 };
	int userSize = 0;
	std::string online = "";
	try
	{
		
		std::cout << "Client accepted !" << std::endl;
		//send(clientSocket, s.c_str(), s.size(), 0);  // last parameter: flag. for us will be 0.


	
		
		while (true)
		{
			int msgCode = (Helper::getMessageTypeCode(clientSocket));
			if (msgCode == LOGINMSG) {
				//lock_guard<mutex> guard(mtx);
				std::unique_lock<std::mutex> lck(mtx);
				string name_len = Helper::getStringPartFromSocket(clientSocket, 2);
				string name = Helper::getStringPartFromSocket(clientSocket, std::atoi(name_len.c_str()));
				std::cout << "Client name is: " << name << std::endl;
				client = name;
				this->Users.push_back(client);
				for (int i = 0; i < this->Users.size(); i++)
				{
					online += this->Users[i] + "&";
				}
				userSize = this->Users.size();
				Helper::send_update_message_to_client(clientSocket, client, client, online);
				condVar.notify_all();
			}
			else if(msgCode == ClientUpdateMsg)
			{
				
				string name_len = Helper::getStringPartFromSocket(clientSocket, 2);
				string name = Helper::getStringPartFromSocket(clientSocket, std::atoi(name_len.c_str()));
				string msg_len = Helper::getStringPartFromSocket(clientSocket, 5);

				string msg = Helper::getStringPartFromSocket(clientSocket, std::atoi(msg_len.c_str()));
				online = "";
				for (int i = 0; i < this->Users.size(); i++)
				{
					online += this->Users[i] + "&";
				}
				online[online.size() - 1] = 0;
				if (name != "")
				{
					Helper::Update(clientSocket, client, name, online);
					Helper::Update(clientSocket, name, client, online);
				}
				if (msg != "") {
					std::cout << msg << std::endl;
					std::cout << name << std::endl;
					std::cout << online << std::endl;
					Helper::send_update_message_to_client(clientSocket, msg, name, online);
				}
				Helper::saveMsg(client, name, msg);
				condVar.notify_all();

				//Helper::send_update_message_to_client(clientSocket, name, msg, online);
			}

			std::unique_lock<std::mutex> lck(mtx);
			condVar.wait(lck);
			online = "";
			for (int i = 0; i < this->Users.size(); i++)
			{
				online += this->Users[i] + "&";
			}
			userSize = this->Users.size();
			Helper::send_update_message_to_client(clientSocket, "", client, online);
			
		}
	}
	catch (const std::exception& e)
	{
		erase(this->Users, client);
		condVar.notify_all();
		online = "";
		for (int i = 0; i < this->Users.size(); i++)
		{
			online += this->Users[i] + "&";
		}
		userSize = this->Users.size();
		Helper::send_update_message_to_client(clientSocket, "", (string)this->Users[userSize - 1], online);
		std::cout << client << " dissconnected!" << std::endl;
		
		closesocket(clientSocket);
	}


}

